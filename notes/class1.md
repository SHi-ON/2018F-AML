# Newsvendor Problem 

We discussed the newsvendor problem and possible model-free solutions for it.  The model-free algorithm is in [modelfree.py](../solutions/newsvendor/modelfree.py).

We also gathered useful python references in [python_resources](../python/python_resources.md)

