
# Convex optimization library:
# For examples, see https://cvxr.rbind.io/post/cvxr_examples/
library(CVXR)
library(knitr)

# from http://www.sumsar.net/blog/2014/03/a-hack-to-create-matrices-in-R-matlab-style/
# does string parsing: make sure to not use | for anything else but a row separator
qm<-function(...)
{
  # turn ... into string
  args<-deparse(substitute(rbind(cbind(...))))
  
  # create "rbind(cbind(.),cbind(.),.)" construct
  args<-gsub("\\|","),cbind(",args)
  
  # eval
  # marek: added envir=parent.frame(1) to access local function args
  eval(parse(text=args),envir=parent.frame())
}

#' A matrix of all ones
ones <-function(nrow, ncol){matrix(1,nrow,ncol)}
#' A matrix of all zeros
zeros <-function(nrow, ncol){matrix(0,nrow,ncol)}

students <- c("Marek",	"Shayan",	"Sai",	"Kaixin",	"Soheil",
               "Amith",	"Ram",	"Peihao",	"Nick",	"Yi",	"Xin",	"Alison")

papers <- c("Tax Collection Optimization",
            "Solving Go",
            "Distributional Bellman Operator",
            "Model-based Interval Estimation for MDPs",
            "PAC Optimal MDP Planning",
            "Safe Policy Improvement",
            "Performance Loss Bounds",
            "Least Squares Temporal Difference",
            "Approximate Linear Programming",
            "Least-Squares Policy Iteration",
            "Optimizer's curse",
            "Policy Gradient Methods")
paper.order <- c(1, 2, 12, 7,8,9,10,4,6,5,3,11)

# row is a paper and columns is a presenter
pref <- 
qm( 1, 2,	3,	2,	1,	  3,	1,	2,	2,	3,	3,	1   |
    1, 3,	3,	3,	1,	  3,	3,	3,	3,	3,	2,	3   |
    1, 2,	2,	2,	2,	  1,	1,	2,	3,	1,	3,	3   |
    3, 1,	2,	1,	2,	  2,	1,	1,	2,	1,	1,	2.1 |
    1, 3,	1,	3,	3,	  1,	2,	1,	1,	1,	3,	2   |
    1, 3,	2,	2,	3,	  2,	1,	3,	2,	1,	2,	3   |
    3, 2,	1,	2,	1,	  1,	2,	2,	2,	1,	2,	1   |
    3, 1,	2,	1,	2.1,	2,	1,	2,	1,	2,	1,	1   | 
    1, 3,	2,	3,	1,	  1,	2,	1,	3,	1,	2,	2   |
    1, 1,	2,	1,	3,	  3,	3,	1,	2,	1,	2,	1   |
    1, 3,	2,	3,	1,	  2,	1,	1,	2,	1,	3,	1   |
    1, 2,	2,	2,	3,	  3,	2,	2,	3,	2,  2,	2)


A <- Int(nrow(pref), ncol(pref))

obj <- Maximize(ones(1,nrow(A)) %*% (A * pref) %*% ones(ncol(A),1))

constraints <- list(
  A >= 0,
  A <= 1,
  ones(1,nrow(A)) %*% A == 1,
  A %*% ones(ncol(A),1) == 1)

problem <- Problem(obj, constraints)

sol <- solve(problem)
print(sol$val)
assgn <- sol$getValue(A)

print(round(assgn))

assignments <- data.frame(
  Paper = papers,
  Presenter = students[apply(assgn, 1, which.max)]
)


print(kable(assignments[order(paper.order),],row.names = FALSE) )


