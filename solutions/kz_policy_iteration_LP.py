# Kaixin Zhang
# Policy iteration - Ortools Linear Programming solver

from ortools.linear_solver import linear_solver_pb2
from ortools.linear_solver import pywraplp


def policy_iteration():
    solver = pywraplp.Solver(
        "LP-Policy Iteration", pywraplp.Solver.GLOP_LINEAR_PROGRAMMING
    )

    S = ["El", "La", "Err"]

    P = [["El", 0.8, 0.2], ["La", 0.4, 0.6], ["Err", 0.0, 0.0]]  # Nothing

    # Action name, El, La, Err
    r = [["Keep", 200, -20, 0], ["Sell", 6300, 6300, 0]]

    gamma = 1 / 1.02

    # Create Objective
    decisions = []
    # For each action create a value v
    values = []
    for i in range(0, len(r)):
        values[i] = solver.Objective()
        # For each state, define a decisions that get the maximum value v

        for j in range(len(S)):
            decisions[j] = solver.NumVar(0.0, solver.infinity(), r[j][0])
            values[i].SetCoefficient(r[i][1], P[j][1])
            values[i].SetCoefficient(r[i][2], P[j][2])

        values[i].SetMaximization()

    # Create constraints
    # Stop when policy doesn't change
    constraints = None
    constraint = solver.Constraint()


if __name__ == "__main__":
    policy_iteration()
