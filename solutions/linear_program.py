from gurobipy import *

m = Model("mdp")

m.setObjective(
    state1_action1 * 0.8 * 200/(1-1.02) + state1_action1 * 0.2 * -20/(1-1.02)
    state1_action2 * 6300/(1-1.02) +
    state2_action1 * 0.4 * 200/(1-1.02) + state2_action1 * 0.6 * -20/(1-1.02)
    state2_action2 * 6300/(1-1.02))


state1_action1 = m.addVar(vtype=GRB.CONTINUOUS, name="state1_action1")
state1_action2 = m.addVar(vtype=GRB.CONTINUOUS, name="state1_action2")
state2_action1 = m.addVar(vtype=GRB.CONTINUOUS, name="state2_action1")
state2_action2 = m.addVar(vtype=GRB.CONTINUOUS, name="state2_action2")


m.addConstr(state1_action1 * 1 + state1_action2 * 1 = 1, "c0")
m.addConstr(state2_action1 * 1 + state2_action2 * 1 = 1, "c1")


m.optimize()
