#Xin Zhang
#Implement Liner Programming
from ortools.linear_solver import pywraplp
import numpy as np

## create an liner programming, named "lp"
solver = pywraplp.Solver('lp', pywraplp.Solver.GLOP_LINEAR_PROGRAMMING)

gamma = 1 / 1.02

## create variables, need to figure out

## create objective function, is value function
objective = solver.Objective()

objective.SetMaximization()

## create Constraint, need to figure out