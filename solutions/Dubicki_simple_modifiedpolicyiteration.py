# -*- coding: utf-8 -*-
"""
Created on Fri Sep 14 10:11:42 2018

@author: Nick
"""


# -*- coding: utf-8 -*-
"""
Created on Sat Sep 15 15:02:38 2018

@author: Nick
"""
import numpy as np
## Two states with a decision at any time

# TODO: use fractions even when they are integral
# dimensions: action, state from, stateto
P = np.array([[[0.8, 0.2, 1.0],[0.4, 0.6, 1.0], [0.0, 0.0, 1.0] ], 
              [[0.0, 0.0, 1.0],[0.0, 0.0, 1.0], [0.0, 0.0, 1.0] ]])


# dimensions: action, state
r = np.array([[200,-20, 0], [6300, 6300, 0]])

## Modified POLICY iteration

assert P.shape[0] == r.shape[0]
assert P.shape[1] == P.shape[2]
assert P.shape[1] == r.shape[1]

gamma = 1/1.02
max_iterations = 10
action_names = ["keep", "sell"]

#The m_n sequence
m_n = 50

#Tolerence
epsilon = 10**-2

#Initialize v_o the value function
v   = np.zeros(  P.shape[2] )
p_d = np.zeros( (P.shape[1], P.shape[2]) )
r_d = np.zeros(  P.shape[1] )
d_o = np.zeros(  P.shape[2] )#Decision maps state into action

for n in range(max_iterations):
    
    #Set new decision according to argmax
    for s in range( P.shape[1] ):     
        qval = np.array([r[i,:] + gamma * P[i,:,:] @ v for i in range(P.shape[0])])    
        d    = np.argmax(qval, axis=0)
        
        
    #Apply decision to Transition matrix by row replacement
    for s in range( P.shape[1] ):
        #print(s)
        #print(P[s])
        p_d[s,:] = P[ d[s], s, : ]
        r_d[s]   = r[ d[s], s ]   
        
        
#    #is identical to previous?
#    #else store current value for later
#    if np.all( d_o == d ):
#        break;
#    else:
#        d_o = d
#        
    d_o = d
        
    #corresponding m from the sequence {m_n}
    m = m_n
    
    #Init u
    uu = np.array([ r[i,:] + gamma*P[i,:,:]@v for i in range(P.shape[0]) ] )
    u  = np.max( uu, axis=0 )
    

    
    #Commence Partial Policy Eval
    if(  np.max( np.abs( u - v ) ) < epsilon*( 1-gamma )/(2*gamma) ):
        #value function is within tolerence.
        #Therefore d is truly optimal within tolerance
        
        break;
        
    else:
        #Update u
        
        for k in range( m ):
            u = r_d + gamma*p_d @ u
            
        
    v = u
    
#Complete loop
#----------------------------------------------------   
 
print( "Policy:", list(np.take(action_names, d)) )
print("Value function:", v)