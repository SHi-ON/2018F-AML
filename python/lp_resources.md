# Linear Programming in Python 

- [Benchmarks of Simplex solvers](http://plato.asu.edu/ftp/lpsimp.html)
- [Benchmarks of Commercial Barrier](http://plato.asu.edu/ftp/lpcom.html)

## Solvers with convenient python interfaces

These solvers are also free or free for academia. Biased towards the ones I have experience with.

- [Mosek](https://www.mosek.com/documentation/)
- [Gurobi](https://www.gurobi.com/documentation/8.0/quickstart_windows/py_python_interface)
- [CVXpy](http://www.cvxpy.org/)
- [Google optimization suite](https://developers.google.com/optimization/)
