\documentclass{beamer}
\usepackage{graphicx}

\title{Advanced Topics in Machine Learning}
\subtitle{Lecture 1}
\author{Marek Petrik}

\usecolortheme{beaver}
\useinnertheme{circles}
%\usecolortheme{rose}
%\setbeamercovered{transparent}
\setbeamercovered{invisible}
\usefonttheme{professionalfonts}
\setbeamertemplate{navigation symbols}{}


\newcommand{\tc}[2]{\textcolor{#1}{#2}}
\newcommand{\tcb}[1]{\tc{blue}{#1}}
\newcommand{\tcr}[1]{\tc{red}{#1}}
\newcommand{\tcg}[1]{\tc{green}{#1}}
\newcommand{\frep}{\tilde{\mathcal{U}}}
\newcommand{\freqs}{\mathcal{U}}

\newcommand{\fre}{d}


\renewcommand{\P}{\mathbb{P}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\nat}{\xi}
\newcommand{\natures}{\Xi}
\newcommand{\timesteps}{\mathcal{T}}
\newcommand{\return}{\rho}
\newcommand{\measures}[1]{\triangle^{#1}}
\newcommand{\rewset}{\mathcal{R}}
\newcommand{\tpol}{\bar\pol}
\newcommand{\transtate}{p}
\newcommand{\ttranstate}{\bar{\transtate}}
\newcommand{\ambigs}{\mathcal{B}}
\newcommand{\dif}{\operatorname{d}\!}
\newcommand{\ext}{\operatorname{ext}}
\newcommand{\tactions}{\bar\actions}
\newcommand{\tval}{\bar\val}
\newcommand{\opt}{^{\star}}

\begin{document}

\begin{frame}
	\maketitle
\end{frame}
%
%\begin{frame}
%  	\tableofcontents
%\end{frame}
%
%\section{Logistics}
%
%\begin{frame}
%	\tableofcontents[currentsection]
%\end{frame}

\begin{frame} \frametitle{Introductions}
What is your:
\begin{itemize}
	\item Background, PhD/MS student, year, department
	\item Research / project interest 
	\item Job aspirations
	\item Non-academic interest
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Advanced ML: Reinforcement Learning}
	\begin{itemize}
		\item Seminar class:
		\begin{itemize}
			\item Loosely organized			
			\item Depth rather than breadth
			\item Cover difficult not most popular topics
		\end{itemize} 
		\vfill
		\item \alert{Website}: \url{https://gitlab.com/marekpetrik/2018F-AML}
		\item \alert{Email} Marek: \href{mpetrik@cs.unh.edu}{mpetrik@cs.unh.edu}
		\item \alert{Assignments}:  \url{mycourses.unh.edu} 
	\end{itemize}	
\end{frame}

\begin{frame} \frametitle{Goals of the Seminar}
	\begin{enumerate}
		\item Learn how to study independently
		\vfill
		\item Understand reinforcement learning
		\vfill
		\item Learn how to read research papers
		\vfill		
		\item Do research and discover something
	\end{enumerate}
\end{frame}

\begin{frame}{Class Expectations}
\begin{itemize}
	\item \alert{Present a paper and lead the discussion}: Important to do well, make slides (about 1.3 papers)
	\vfill
	\item \alert{Read papers}: read each paper, submit a response and answer several easy questions (12 papers)
	\vfill	
	\item \alert{Prototype RL algorithms}: Focus on correctness and ease of implementation (about 3 algorithms in groups of 3)
	\vfill	
	\item \alert{Class project}: Use RL to mitigate spread of invasive species (simulated)
\end{itemize}
\end{frame}

\begin{frame}{Class Content: Foundations of Reinforcement Learning}
\begin{enumerate}
	\item \alert{Foundations text} of Markov Decision processes
	\item \alert{Scaling up}: Solving large MDPs
	\item \alert{Using data}: Uncertainty, robustness, and exploration
\end{enumerate}
\vfill
\alert{Tools}: Linear algebra, mathematical optimization, statistics
\vfill
See the schedule at  \url{https://gitlab.com/marekpetrik/2018F-AML}
\end{frame}

\begin{frame}{Programming Language}
Need to pick one of:
\begin{enumerate}
	\item R
	\item Python
	\item C++
\end{enumerate}
Most important for the group implementation assignments
\vfill
Classes will be interactive if possible
\end{frame}

\begin{frame}{Where Was This Picture Taken?}
\centering
About 9 am on 9/2/2017 \\
\includegraphics[width=\linewidth]{photo.jpg}
\end{frame}

\begin{frame}{Markov Decision Process (MDP)}
\begin{itemize}
	\item Models dynamic decision making in RL
	\item What to do \alert{now}, given that:
	\begin{enumerate}
		\item I learn more in the future
		\item I can take more actions in the future
		\item Rewards happen in the future
	\end{enumerate}
\end{itemize}
\end{frame}

\begin{frame}{MDP Components}
\begin{itemize}
	\item \alert{States}
	\item \alert{Actions}
	\item \alert{Transition probabilities}
	\item \alert{Rewards}
\end{itemize}
\vfill
\alert{Objective}: Maximize (some) sum of rewards \\
\begin{itemize}
	\item Finite horizon
	\item Infinite horizon:
	\begin{itemize}
		\item Discounted
		\item Stochastic shortest path (or total return)
		\item Average
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Solution: Policy}
Depends on:
\begin{itemize}
	\item Stationary policies: State
	\item Markovian policies: State and time
	\item History-dependent policies: History
\end{itemize}
\vfill
Action:
\begin{itemize}
	\item Deterministic
	\item Randomized
\end{itemize}
\end{frame}

\begin{frame}{Markov Property}
\emph{Future} state is independent of the \emph{present} state conditional on the \emph{present} state. Or more more clearly:
\pause
\vfill
Assume a probability space $(\Omega, \mathcal{F}, \P)$ \pause and a filtration $(\mathcal{F}_s, s\in \N)$ and a measurable space $(S,\mathcal{S})$. \pause
\vfill
A discrete stochastic process $\{ X_t : \Omega \rightarrow S\}_{t\in \N}$ adapted to the filtration has the \emph{Markov property} if for each $n$:
\begin{gather*} \P [X_n = x_n ~|~ X_{n-1} = x_{n-1} ]  = \\ = \P [X_n = x_n ~|~ X_{n-1} = x_{n-1}, X_{n-2} = x_{n-2}, \ldots, X_{1} = x_{1} ] 
\end{gather*} 
\end{frame}

\begin{frame}{Example Problems}
	\begin{enumerate}
		\item Inventory control
		\item The secretary problem: hiring from a set of candidates
		\item Playing slot machines
		\item Managing a fishery
		\item Managing a forest
		\item Playing chess (or Go, or backgammon, or \ldots)
	\end{enumerate}
\end{frame}

\begin{frame}{What are the challenges?}

\end{frame}

\begin{frame}{Assignments for Wed}
\begin{enumerate}
	\item Rate papers to present at: \url{https://docs.google.com/spreadsheets/d/1Ba6MeCUOgFwt73rk-1qiXSUOWwoAr6ndb67evq5A_ZM/edit?usp=sharing}
	\vfill
	\item Read up on Newsvendor problem
	\vfill
	\item Look at the simulator: \url{http://rl2.cs.unh.edu:3838/cs980/newsvendor/}
\end{enumerate}
\end{frame}

\end{document}